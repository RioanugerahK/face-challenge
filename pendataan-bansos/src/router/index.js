import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta : {
      title: "Formulir pendataan Bantuan"
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/Form.vue')
  },
  {
    path: '/info',
    name: 'Info',
    meta: {
      title : "Info Pendataan"
    },
    props: true ,
    component: () => import(/* webpackChunkName: "about" */ '../views/Info.vue')
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
