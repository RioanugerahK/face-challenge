Informasi nama peserta, kelas belajar.
Nama    : RIO ANUGERAH KUSBIANTO
KELAS   : Vue.Js   

Link video/record screen demo yang berisi penjelasan singkat hasil pengujian aplikasi (pastikan bisa dibuka tanpa login).
https://youtu.be/a11gNuEZ3Dg (part1)
https://youtu.be/OCm45XiEVWQ (part2)
Link deploy di hosting (Github Pages/Netlify/Surge/Render).
https://idyllic-dusk-f2c016.netlify.app/   (Deploy Menggunakan Netlify)

1.Tentang Layout/Desain

Desain yang saya buat sangat sederhana, sedikit warna dan tidak memiliki pola, dengan harapan supaya mudah digunakan oleh orang awaw. Desain sekilas mirip dengan google form namun saya modifikasi dibeberapa bagian dan saya gabungkan desain nya dengan desain default app dari Vuetify ,  dan saya hanya menambahkan gambar logo jabar Digital service di dalam bagian side bar atau navigation .

2.Kolom Input
Saya menggunakan jenis kolom input dengan posisi label berada diatas dan berukuran lebih kecil daripada Kolom Input, yang bertujuan supaya pengguna lebih mudah melihat mana kolom untuk diisi dan mana label.
Dalam mempermudah pengisian sama menggunakan placeholder yang berisikan contoh atau keterangan apa yang harus diisikan atau apa yang harus dilakukan pengguna untuk mengisi kolom tersebut.

3.Validasi Form
Dalam memvalidasi form saya menggunakan validasi langsung dan tidak langsung :

(A) Validasi langsung, validasi langsung yang mana ketika data yang diinput kurang tepat maka kolom akan merespon dan memberitahukan apa kesalahannya dibawah kolom input yang sedang aktif.

(B)Validasti tidak langsung, validasi ini saya gunakan ketika pengguna menekan tombol simpan. Sistem akan melakukan validasi terhadap seluruh kolom input apakah sudah sesuai atau belum dan jika salah akan menampilkan informasi kesalahannya dibawah kolom input yang tidak sesuai.

4.Tipe Notifikasi:

(A) Tipe Dialog, saya menggunakan jenis ini untuk menampilkan informasi gagal menyimpan. Sebab, dengan jenis notifikasi ini akan memudahkan pengguna untuk menekan tombol simpan kembali.
(B) Tipe Pindah Halaman, saya menggunakan tipe ini pada saat data berhasil disimpan. Sebab, dengan begitu pengguna akan fokus dan mengetahui bahwa data yang tadi diinput telah disimpan tanpa terganggu dengan tampilan lainnya.


# pendataan-bansos

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
